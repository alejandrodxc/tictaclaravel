<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PartidaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PartidaController::class, 'index'])->name('index');
Route::get('/newName', [PartidaController::class, 'newName'])->name('newName');
Route::any('/new', [PartidaController::class, 'new'])->name('new');
Route::get('/old', [PartidaController::class, 'old'])->name('old');
Route::post('/jugada', [PartidaController::class,'jugada'])->name('jugada');

