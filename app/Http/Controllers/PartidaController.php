<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PartidaController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function newName()
    {
        return view('games/newName');
    }

    public function old()
    {
        $user = DB::table('partidas')->where(['usuario2' => 0, 'ganador' => 0])->get();
        return view('games/oldGame', compact('user'));
    }

    public function new()
    {


        if (!empty(request()->jugador1)) {
            $user = DB::table('users')->where('name', request()->jugador1)->first();
        }

        if (empty($user) || $user == NULL) {
            $insertUser = DB::table('users')->insert(['name' => request()->jugador1, 'email' => 'email@ejemplo', 'password' => 'generico']);
            $user = DB::table('users')->where('name', request()->jugador1)->first();
        }

        DB::table('partidas')->insert(['usuario1' => $user->id, 'user_id' => $user->id, 'usuario2' => 0, 'ganador' => 0]);
        $last = null;
        $message = "";
        $defaultBoard = [
            [3, 3, 3],
            [3, 3, 3],
            [3, 3, 3]
        ];

        $board = [
            [3, 3, 3],
            [3, 3, 3],
            [3, 3, 3]
        ];

        $winConditions = [
            [[0, 0], [0, 1], [0, 2]],
            [[1, 0], [1, 1], [1, 2]],
            [[2, 0], [2, 1], [2, 2]],
            [[0, 0], [1, 0], [2, 0]],
            [[0, 1], [1, 1], [2, 1]],
            [[0, 2], [1, 2], [2, 2]],
            [[0, 0], [1, 1], [2, 2]],
            [[0, 2], [1, 1], [2, 0]],
        ];
        return view(
            'games/index',
            compact(
                'last',
                'message',
                'defaultBoard',
                'board',
                'winConditions',
                'user'
            )
        );
    }


    public function checkWinner($conditions, $response)
    {

        $lr = null;
        $matches = [];
        for ($i = 0; $i <= count($conditions) - 1; $i++) {
            foreach ($conditions[$i] as $rows) {
                $x = $rows[0];
                $y = $rows[1];
                if ($response[$x][$y] != 3) {
                    if ($lr == $response[$x][$y] || $lr == null) {
                        $matches[] = $response[$x][$y];
                        $lr = $response[$x][$y];
                    } else {
                        $lr = null;
                        $matches = [];
                        continue;
                    }
                }
            }
            if (count($matches) == 3) {
                if ($matches[0] == $matches[1] && $matches[1] == $matches[2]) {
                    return true;
                } else {
                    $matches = [];
                    $lr = null;
                }
            } else {
                $matches = [];
                $lr = null;
            }
        }
        return false;
    }


    public function jugada()
    {

        $user = DB::table('users')->where('name', request()->jugador1)->first();
        $message = '';
        $winConditions = [
            [[0, 0], [0, 1], [0, 2]],
            [[1, 0], [1, 1], [1, 2]],
            [[2, 0], [2, 1], [2, 2]],
            [[0, 0], [1, 0], [2, 0]],
            [[0, 1], [1, 1], [2, 1]],
            [[0, 2], [1, 2], [2, 2]],
            [[0, 0], [1, 1], [2, 2]],
            [[0, 2], [1, 1], [2, 0]],
        ];

        $board = !empty($_POST['board']) ? json_decode($_POST['board']) : [];
        $last = !empty($_POST['last']) ? $_POST['last'] : null;
        $responses = [];
        $rowarray = [];
        $counter = 0;
        $changes = [];


        foreach ($_POST as $key => $value) {
            if (!in_array($key, ["board", "play", 'last', 'jugador1', '_token'])) {
                if ($value == 'O') {
                    $rowarray[] = 0;
                } elseif ($value == 'X') {
                    $rowarray[] = 1;
                } else {
                    $rowarray[] = 3;
                }
                $counter++;

                if ($counter % 3 == 0) {
                    $responses[] = $rowarray;
                    $rowarray = [];
                }
            }
        }

        for ($i = 0; $i <= count($board) - 1; $i++) {
            foreach ($board[$i] as $key => $value) {
                if ($value != $responses[$i][$key]) {
                    $changes[] = $responses[$i][$key];
                }
            }
        }


        if (count($changes) > 1) {
            $message .= "ya hiciste tu jugada";
        } else if ($last !=  null  && $last == $changes[0]) {
            $message .= "no puedes jugar dos veces";
        } else if ($this->checkWinner($winConditions, $responses)) {
            $last = $changes[0];
            $winner = null;
            if ($last == 1) {
                $winner = "X";
            } else if ($last == 0) {
                $winner = "O";
            }
            $board = $responses;
            $message .= 'tenemos un ganador ';
            $message .= "El ganador es:" .  $winner;
        } else {
            $last = $changes[0];
            $board = $responses;
        }



        return view('games/index', compact('last', 'message', 'board', 'winConditions', 'user'));
    }
}
