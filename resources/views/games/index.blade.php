<?php
function createTile($value, $id){
    $o = "";
    $name = "row_". $id;
    $realValue = null;
    if($value == 0){
        $realValue = "O";
    }else if($value == 1){
        $realValue = "X";
    }else{
        $realValue = "select";
    }

    if($value == 0 || $value == 1){
        $o .= "<input type='hidden' name='".$name."' value='".$realValue."'/>";
        $o .= "<select disabled='disabled' class='form-control'>";   
    }else{
        $o .= "<select name='".$name."' id='".$name."' class= 'form-control'>";
    }
    $o .= "<option>select</option>";
    if($value == 0){
        $o .= "<option selected='selected'>O</option>";
    }else{
        $o .= "<option>O</option>";
    }
    if($value ==1){
        $o .= "<option selected='selected'>X</option>";
    }else{
        $o .= "<option>X</option>";
    }
    $o .= "</select>";
    return $o;
}
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
 rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
 crossorigin="anonymous" />

<form  action='/jugada' method='post' class="row row-cols-lg-auto g-3 align-items-center" id='formTicTac'>
<?php if($message):?>
            <p class="tct-message"><?php echo $message;?></p>
        <?php endif;?>
        
       
<div class="mb-3">
  <label for="jugador1" class="form-label">Jugador 1</label>
  <input class='form-control' name='jugador1' id='jugador1' value="<?php echo $user->name; ?>">
</div>
<div class="mb-3">
  <label for="jugador2" class="form-label">jugador2 </label> <br />
  <label for="jugador2" class="form-label"></label>
  </div>
  
  @csrf
        <table class="table">
            <?php $count = 1; ?>
             @foreach($board as $row)
                <tr>
                    @foreach($row as $tile)
                        <td>
                            <?php
                             echo  $tile;
                             echo createTile($tile, $count);
                             ?>
                        </td>
                        <?php $count++; ?>
                        @endforeach
                </tr>
             @endforeach
        </table>
        <br>
        <input name="board" type="hidden" value="<?php echo json_encode($board);?>"/>
        <input name="last" type="hidden" value="<?php echo $last;?>"/>
        <button name="play">End Turn</button>
        <a href="{{route('new')}}" class='button'> Nuevo juego </a>
        
    </form>









