<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


use \App\Models\User;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Partida>
 */
class PartidaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'usuario1' => $this->faker->randomDigit(),
            'usuario2' => $this->faker->randomDigit(),
            'ganador' => $this->faker->randomDigit(),
            'user_id' => User::all()->random(),
            
        ];


        
    }
}
