informacion de proyecto

ip: "192.168.56.10"
memory: 2048
cpus: 2
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/code
      to: /home/vagrant/code

sites:
    
    - map: tictac.test
      to: /home/vagrant/code/tictac/public
      type: "apache"


En el etc host 

192.168.56.10 tictac.test